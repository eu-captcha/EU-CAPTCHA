<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name="viewport">
        <meta content="#000000" name="theme-color">
        <title>EU Captcha</title>
       <!-- <script>if ("ReactSnap" !== navigator.userAgent) {
        var cl = document.querySelector("html").classList;
        cl.remove("no-js"), cl.add("has-js");
        var head = document.head || document.getElementsByTagName("head")[0], cck = document.createElement("script");
        cck.src = "https://ec.europa.eu/wel/cookie-consent/consent.js", cck.type = "text/javascript", head.appendChild(cck)
    }</script> -->
        <script>window.location.hash.match("access_token") && (document.location.pathname += "/admin"), window.location.hash.match("invite_token") && (document.location.pathname += "/admin"), window.location.hash.match("confirmation_token") && (document.location.pathname += "/admin"), window.location.hash.match("email_change_token") && (document.location.pathname += "/admin"), window.location.hash.match("recovery_token") && (document.location.pathname += "/admin")</script>

        <script type="text/javascript" src="https://apmactivegate.tech.ec.europa.eu:443/jstag/managed/39a3e95b-5423-482c-879b-99ef235dffeb/9e3c4a81c651b37_complete.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" media="all" href="css/eu/all.css"/>
        <link rel="stylesheet" media="screen" href="css/eu/screen.css"/>
        <link rel="stylesheet" media="print" href="css/eu/print.css"/>
        <link rel="stylesheet" media="screen" href="css/eu/screen1.css"/>
        <link rel="stylesheet" media="print" href="css/eu/print1.css"/>

        <!--<link href="js/eu/ec.ea4b8b78.chunk.js" rel="prefetch" as="script"> -->
        <!--<link href="js/eu/eu.1f3ba700.chunk.js" rel="prefetch" as="script"> -->

        <meta content="Europa Component Library (ECL) documentation website" name="Description" data-react-helmet="true">
    </head>
    <body class="language-en ecl-typography path-frontpage page-node-type-landing-page">
    <div class="globan globan-invalid-domain"><b>FOR TESTING PURPOSES ONLY. THIS NOTIFICATION WILL DISAPPEAR ONCE DEPLOYED ON A *.europa.eu site</b></div>
    <div class="globan globan-dropdown-collapsed dark logo-flag" id="globan" data-nosnippet="true" style="z-index: 40;">
        <div class="globan-center">
            <div class="globan-content">
                <span>An official website of the European Union</span>
                <span>An official EU website</span>
                <button aria-controls="globan-dropdown-y3hrzir4ic" aria-expanded="false">How do you know?</button>
            </div>
            <div id="globan-dropdown-y3hrzir4ic" class="globan-dropdown" hidden="">
                <p class="wt-paragraph">All official European Union website addresses are in the <b>europa.eu</b> domain.</p>
                <p class="wt-paragraph">
                    <a class="wt-link" href="//europa.eu/european-union/contact/institutions-bodies_en">See all EU institutions and bodies</a>
                </p>
            </div>
        </div>
        <style>#globan{background-color:#eee;padding:0 0 0 16px;position:relative;line-height:inherit;min-height:28px}#globan,#globan *,#globan *:after,#globan *:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#globan:after{content:""!important;display:block;clear:both}#globan *{font-size:14px;font-family:Arial,Verdana;-webkit-box-sizing:border-box;box-sizing:border-box}#globan [hidden]{display:none}#globan .globan-center{display:inline-block;position:relative}#globan .globan-content{min-height:25px;line-height:25px}#globan .globan-content button{text-decoration:none;color:#444;background-color:#eee;font-weight:400;margin:0 0 0 6px;padding:0 7px;position:relative;display:inline-block;height:8px;min-height:28px;cursor:pointer;border:3px solid rgba(255,255,255,0)}#globan .globan-content button:hover,#globan .globan-content button:focus,#globan .globan-content button:active,#globan.dark .globan-content button:hover,#globan.dark .globan-content button:focus,#globan.dark .globan-content button:active,#globan .globan-content button[aria-expanded="true"],#globan.dark .globan-content button[aria-expanded="true"]{background-color:#fff;color:#004494;outline:none}#globan .globan-content button:focus,#globan .globan-content button:active,#globan.dark .globan-content button:focus,#globan.dark .globan-content button:active{border-color:#ffd617;outline:none}#globan .globan-content button:hover:after,#globan .globan-content button:focus:after,#globan .globan-content button:active:after,#globan.dark .globan-content button:hover:after,#globan.dark .globan-content button:focus:after,#globan.dark .globan-content button:active:after,#globan .globan-content button[aria-expanded="true"]:after,#globan.dark .globan-content button[aria-expanded="true"]:after{background:center center / 12px auto no-repeat transparent url(https://europa.eu/webtools/images/chevron-blue.svg?t=1699970532)}#globan.dark,#globan.dark button{background-color:#404040}#globan.dark .globan-content,#globan.dark .globan-content button{color:#fff}#globan .globan-content button:after{content:" "!important;display:inline-block;width:20px;height:8px;-webkit-transition:all .3s;-moz-transition:all .3s;-o-transition:all .3s;-ms-transition:all .3s;transition:all .3s;margin-left:2px;background:center center / 12px auto no-repeat transparent url(https://europa.eu/webtools/images/chevron-grey.svg?t=1699970532)}#globan.dark .globan-content button:after{background:center center / 12px auto no-repeat transparent url(https://europa.eu/webtools/images/chevron-white.svg?t=1699970532)}#globan .globan-content button[aria-expanded="true"]:after{transform:rotate(-180deg)}#globan.logo-flag .globan-content:before{content:" "!important;display:inline-block;width:24px;height:16px;background:center center / 110% auto no-repeat #004494 url(https://europa.eu/webtools/images/flag.svg?t=1699970532);float:left;margin:5.5px 8px 0 0;border:1px solid #004494}#globan.dark.logo-flag .globan-content:before{border-color:#7f99cc}#globan span{display:none}#globan span:first-child{display:inline-block}#globan .globan-dropdown{background-color:#fff;position:absolute;right:0;top:100%;padding:12px 16px;margin:0;max-width:500px;border:1px solid #ccc;box-shadow:0 4px 5px 0 rgba(0,0,0,.4);-webkit-box-shadow:0 4px 5px 0 rgba(0,0,0,.4);-moz-box-shadow:0 4px 5px 0 rgba(0,0,0,.4);color:#444}#globan .globan-dropdown p{padding:0;margin:0;line-height:1.4}#globan .globan-dropdown p:nth-child(2){margin-top:10px}#globan .globan-dropdown a{color:#004494;text-decoration:none}#globan .globan-dropdown a:hover{text-decoration:underline}#globan.reverse .globan-center{float:right;padding-right:10px}#globan.reverse.logo-flag .globan-content:before{float:right;margin:2px 0 0 10px}#globan.reverse .globan-content{text-align:right}#globan.reverse .globan-content a,#globan.reverse .globan-content a:hover,#globan.reverse .globan-content a:focus,#globan.reverse .globan-content a:active{float:left;margin:0 20px 0 0}#globan.reverse .globan-content a:after{float:left;margin:0 10px 0 0}#globan.reverse .globan-dropdown{left:0;right:auto;direction:rtl}#globan.fixed{position:fixed;left:0;top:0;right:0}.globan-invalid-domain{background-color:#ffd617;color:#444;text-align:center;padding:5px 16px;display:block;margin-left:-16px;font-size:14px;font-family:Arial,Verdana}@media (max-width:820px){#globan{padding-left:5px}#globan .globan-center{display:block}#globan.reverse .globan-center{float:none}#globan span:first-child{display:none}#globan span:nth-child(2){display:inline-block;border-top:3px solid rgba(255,255,255,0)}#globan .globan-content a{float:right}}@media (max-width:600px){#globan .globan-content a,#globan .globan-content a:hover,#globan .globan-content a:focus,#globan .globan-content a:active{display:inline-block;width:30px;padding:0;position:relative;white-space:nowrap;float:right;text-indent:40px;overflow:hidden}#globan .globan-content a:after{position:absolute;left:-3px;top:0;margin:0;width:30px}}@media print{#globan{display:none}}@media all{#globan .globan-invalid-domain{background-color:#ffd617;color:#444;text-align:center;padding:5px 16px;display:block;margin-left:-16px}}</style></div>
        <header class="ecl-site-header-standardised" data-ecl-auto-init="SiteHeaderStandardised" data-ecl-has-menu>
        <div class="ecl-site-header-standardised__header">
            <div class="ecl-site-header-standardised__container ecl-container">
                <div class="ecl-site-header__top">
                    <a href="https://ec.europa.eu/info/index_en"
                       class="ecl-link ecl-link--standalone ecl-site-header__logo-link"
                       aria-label="European Union">
                        <img alt="European Union flag"
                             title="European Union"
                             class="ecl-site-header__logo-image ecl-site-header__logo-image-desktop"
                             src="css/images/logo-eu--en.e7dcfe79.svg"/>
                    </a>
                </div>
            </div>
        </div>
        <div class="ecl-site-header-standardised__banner">
            <div class="ecl-container">
                <div class="ecl-site-header-standardised__site-name">EU CAPTCHA</div>
            </div>
        </div>
        <div id="block-ewcms-theme-horizontal-menu">
            <nav class="ecl-menu ecl-menu--group1" data-ecl-menu data-ecl-auto-init="Menu" aria-expanded="false">
                <div class="ecl-menu__overlay" data-ecl-menu-overlay></div>
                <div class="ecl-container ecl-menu__container">
                    <a class="ecl-link ecl-link--standalone ecl-menu__open" href="" data-ecl-menu-open>
                        Menu
                    </a>
                    <section class="ecl-menu__inner" data-ecl-menu-inner>
                        <header class="ecl-menu__inner-header">
                            <button class="ecl-menu__close ecl-button ecl-button--text" type="submit"
                                    data-ecl-menu-close>
                                <span class="ecl-menu__close-container ecl-button__container">
                                    <span class="ecl-button__label" data-ecl-label="true">Close</span>
                                </span>
                            </button>
                            <div class="ecl-menu__title">Menu</div>
                            <button data-ecl-menu-back type="submit" class="ecl-menu__back ecl-button ecl-button--text">
                                <span class="ecl-button__container">
                                    <!-- <svg class="ecl-icon ecl-icon--s ecl-icon--rotate-270 ecl-button__icon ecl-button__icon--before"
                                         focusable="false" aria-hidden="true" data-ecl-icon>
                                        <use xlink:href="/component-library/dist/media/icons.cec58484.svg#corner-arrow">
                                        </use>
                                    </svg> -->
                                    <span class="ecl-button__label" data-ecl-label>Back</span>
                                </span>
                            </button>
                        </header>
                        <ul class="ecl-menu__list">
                            <li class="ecl-menu__item ecl-menu__item--current" data-ecl-menu-item>
                                <a href="/" class="ecl-menu__link ecl-menu__link--current" data-ecl-menu-link>Home </a>
                            </li>
                        </ul>
                    </section>
                </div>
            </nav>
        </div>
    </header>
        <div class="ecl-page-header-standardised ecl-u-pb-none">
            <div class="ecl-container">
            <nav class="ecl-breadcrumb-standardised ecl-u-pt-none" aria-label="" data-ecl-breadcrumb-standardised="true">
                <ol class="ecl-breadcrumb-standardised__container">
                    <li class="ecl-breadcrumb-standardised__segment" data-ecl-breadcrumb-standardised-item="static">
                        <a href="https://european-union.europa.eu/index_en"
                           class="ecl-link ecl-link--standalone ecl-link--no-visited ecl-breadcrumb-standardised__link">
                            European Union
                        </a>
                        <!-- <svg class="ecl-icon ecl-icon--2xs ecl-icon--rotate-90 ecl-breadcrumb-standardised__icon"
                            focusable="false" aria-hidden="true" role="presentation">
                            <use xlink:href="css/images/icons.cec58484.svg#corner-arrow">
                            </use>
                        </svg> -->
                    </li>
                    <li class="ecl-breadcrumb-standardised__segment" data-ecl-breadcrumb-standardised-item="static">
                        <a href="" class="ecl-link ecl-link--standalone ecl-link--no-visited ecl-breadcrumb-standardised__link">
                            EU CAPTCHA
                        </a>
                        <!-- <svg class="ecl-icon ecl-icon--2xs ecl-icon--rotate-90 ecl-breadcrumb-standardised__icon"
                            focusable="false" aria-hidden="true" role="presentation">
                            <use xlink:href="css/images/icons.cec58484.svg#corner-arrow">
                            </use>
                        </svg> -->
                    </li>
                </ol>
            </nav>
            <div class="ecl-page-banner__container">
                <div class="ecl-page-banner__content">
                    <div class="ecl-page-banner__title">EU CAPTCHA</div>
                    <p class="ecl-page-banner__description">EU CAPTCHA is a test intended to distinguish human from
                        machine input. EU CAPTCHA prevents the use of bots/machines (e.g. for automated account creation) of
                        externally-visible services provided by EU Institutions. The EU CAPTCHA managed service is secure,
                        user friendly and multilingual.
                    </p>
                </div>
            </div>
        </div>
        </div>
        <main class="ecl-u-pb-xl" id="main-content" data-inpage-navigation-source-area="h2.ecl-u-type-heading-2">
        <div class="ecl-container">
            <div class="ecl-row">
                <div class="ecl-col-s-12">
                    <div id="block-ewcms-theme-main-page-content" data-inpage-navigation-source-area="h2" class="ecl-u-mb-l">
                        <article role="article">
                            <div>
                                <div class="ecl-u-mb-2xl">
                                    <h2 class="ecl-u-type-heading-2">What is EU CAPTCHA?</h2>
                                    <div class="ecl">
                                        <p>EU CAPTCHA embodies the results of
                                            <a href="https://ec.europa.eu/isa2/actions/developing-open-source-captcha_en">
                                                ISA² Action 2018.08 EU-Captcha
                                            </a>.
                                            A CAPTCHA is a test intended to distinguish human from machine input.
                                            The objective of this action is to offer to the Member States a CAPTCHA
                                            solution that is maintained, secure, user friendly and multilingual.
                                            It will be delivered as a component that can be operated as a service.
                                            Next to this managed service, there is also an open source project available,
                                            which you can deploy yourself. More information can be found
                                            <a href="https://code.europa.eu/eu-captcha/EU-CAPTCHA/">here</a>
                                        </p>
                                        <p>
                                        EU CAPTCHA supports three types of CAPTCHA:

                                            <li><a href="/textual">Alphanumeric CAPTCHA with audio transcription;</a></li>
                                            <li><a href="/rotate">Image rotation CAPTCHA.</a></li>
                                            <!-- <li><a href="/sliding">Sliding CAPTCHA.</a></li> -->
                                        </p>
                                    </div>
                                </div>
                                <div class="ecl-u-mb-2xl">
                                <h2 class="ecl-u-type-heading-2">Installation</h2>
                                <div class="ecl">
                                    <p dir="auto">EU CAPTCHA can be installed in two ways:</p>
                                    <ul dir="auto">
                                        <li>by integrating with the managed service;</li>
                                        <li>by deploying the code (.WAR file) on your
                                            server/environment.</li>
                                    </ul>
                                    <p dir="auto">Manuals and documentation for both installation
                                        methods can be found in the respective
                                        <a href="https://code.europa.eu/eu-captcha/EU-CAPTCHA/">
                                            GitLab folder
                                        </a>.
                                    </p>
                                    <p dir="auto">
                                        Would you like to know more about the managed service? Please reach out to us:
                                    </p>
                                    <ul>
                                        <li>
                                            Are you working for the European Commission? <br>
                                            Please send an email to <a href="mailto:ec-helpdesk-it@ec.europa.eu">EC-HELPDESK-IT@ec.europa.eu</a>
                                        </li>
                                        <li>
                                            Are you working for a different EU Institution, Body or Agency? <br>
                                            Please send an email to <a href="mailto:EC-CENTRAL-HELPDESK@ec.europa.eu">EC-CENTRAL-HELPDESK@ec.europa.eu</a>
                                        </li>
                                    </ul>
                                    <p>
                                        Please make sure to mention “EU CAPTCHA” in your request.
                                    </p>
                                </div>
                            </div>
                                <div class="ecl-u-mb-2xl">
                                <h2 class="ecl-u-type-heading-2">Objectives of the EU CAPTCHA</h2>
                                <div class="ecl">
                                    <p dir="auto">A CAPTCHA is a test intended to distinguish human from machine
                                        input in order to thwart spam and automatic submission or extraction of
                                        data. The user is typically challenged to solve a puzzle that relies on
                                        expected capacities of the human brains but whose resolution is complex
                                        to automate. Users and, in particular, disabled people are known to
                                        dislike CAPTCHAs that are perceived as hindrances. However, no better
                                        solution was found so far to protect information systems against
                                        malicious automated processes. The characteristics of a good CAPTCHA
                                        are:
                                    </p>
                                    <ul dir="auto">
                                        <li>Security – The number of non-human users able to solve the puzzle
                                            and therefore wrongly identified as being human must be minimised,
                                            which implies that the puzzle should be highly complex to automate;
                                        </li>
                                        <li>User friendliness – The number of human users unable to solve the
                                            puzzle and therefore wrongly identified as being non-human must be
                                            minimised, which implies that the puzzle should be very easy to
                                            solve in a short timeframe by any human being.
                                        </li>
                                    </ul>
                                    <p dir="auto">Several CAPTCHA solutions exist on the market, either provided as
                                        components or as services. Unfortunately, they all have one or more of the
                                        following shortcomings:
                                    </p>
                                    <ul dir="auto">
                                        <li>They provide an insufficient level of security with a high
                                            rate of false positives;
                                        </li>
                                        <li>They provide an insufficient level of user friendliness with
                                            a high rate of false negatives;
                                        </li>
                                        <li>They are not or insufficiently maintained;</li>
                                        <li>They do not support internationalisation or multilingualism
                                            and, in particular, they do not support all official
                                            languages of the European Union;
                                        </li>
                                        <li>They do not support users with disabilities;</li>
                                        <li>They do not have a licensing model that is compatible with
                                            EUPL and, in particular, they cannot be distributed as part
                                            of systems provided by public administrations;
                                        </li>
                                        <li>They raise ethical concerns because they collect private
                                            data or provide puzzles whose resolution creates commercial
                                            value.
                                        </li>
                                    </ul>
                                    <p dir="auto">The objective of the action is to provide a CAPTCHA service that
                                        is:
                                    </p>
                                    <ol dir="auto">
                                        <li>available as a component and operable as a service;</li>
                                        <li>secure;</li>
                                        <li>user friendly;</li>
                                        <li>multilingual with support for all official languages from
                                            the European Union;
                                        </li>
                                        <li>accessible by users with disabilities;</li>
                                        <li>compliant with data protection rules and best practices;
                                        </li>
                                        <li>maintained with continuous support for subsequent versions
                                            of the Java Virtual Machine.
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </main>
        <footer class="ecl-footer-standardised">
        <div class="ecl-container ecl-footer-standardised__container">
            <div class="ecl-footer-standardised__row">
                <div class="ecl-footer-standardised__column">
                    <div class="ecl-footer-standardised__section">
                        <h2 class="ecl-footer-standardised__title">
                            <a href="Index.html" class="ecl-link ecl-link--standalone ecl-footer-standardised__title-link">
                                EU CAPTCHA
                            </a>
                        </h2>
                        <div class="ecl-footer-standardised__description">
                            This site is managed by the European Commission, Directorate-General for Informatics
                        </div>
                    </div>
                </div>
                <div class="ecl-footer-standardised__column"></div>
                <div class="ecl-footer-standardised__column"></div>
            </div>
            <div class="ecl-footer-standardised__row">
                <div class="ecl-footer-standardised__column">
                    <div class="ecl-footer-standardised__section">
                        <a href="https://european-union.europa.eu/index_en"
                           class="ecl-link ecl-link--standalone ecl-footer-standardised__logo-link"
                           aria-label="Home&#x20;-&#x20;European&#x20;Union">
                            <img alt="European Union flag" title="European Union"
                                 class="ecl-footer-standardised__logo-image-mobile"
                                 src="css/images/logo-eu--en.e7dcfe79.svg"/>
                            <img alt="European Union flag" title="European Union"
                                 class="ecl-footer-standardised__logo-image-desktop"
                                 src="css/images/logo-eu--en.e7dcfe79.svg"/>
                        </a>
                        <div class="ecl-footer-standardised__description">
                            Discover more on
                            <a href="https://europa.eu/" class="ecl-link ecl-link--standalone">europa.eu</a>
                        </div>
                    </div>
                </div>
                <div class="ecl-footer-standardised__column">
                    <div class="ecl-footer-standardised__section">
                        <h2 class="ecl-footer-standardised__title ecl-footer-standardised__title--separator">
                            Contact the EU
                        </h2>
                        <ul class="ecl-footer-standardised__list">
                            <li class="ecl-footer-standardised__list-item">
                                <a href="tel:0080067891011"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Call us 00 800 6 7 8 9 10 11
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/contact-eu/call-us_en"
                                class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Use other telephone options
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/contact-eu/write-us_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Write to us via our contact form
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/contact-eu/meet-us_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Meet us at one of the EU centres
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="ecl-footer-standardised__section">
                        <h2 class="ecl-footer-standardised__title ecl-footer-standardised__title--separator">
                            Social media
                        </h2>
                        <ul class="ecl-footer-standardised__list">
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/contact-eu/social-media-channels_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Search for EU social media channels
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="ecl-footer-standardised__section">
                        <h2 class="ecl-footer-standardised__title ecl-footer-standardised__title--separator">
                            Legal
                        </h2>
                        <ul class="ecl-footer-standardised__list">
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/languages-our-websites_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Languages on our websites
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/privacy-policy_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Privacy policy
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/legal-notice_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Legal notice
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/cookies_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Cookies
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/web-accessibility-policy_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Accessibility
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ecl-footer-standardised__column">
                    <div class="ecl-footer-standardised__section">
                        <h2 class="ecl-footer-standardised__title ecl-footer-standardised__title--separator">
                            EU institutions
                        </h2>
                        <ul class="ecl-footer-standardised__list">
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://www.europarl.europa.eu/portal/"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Parliament
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://www.consilium.europa.eu/en/european-council/"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Council
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://www.consilium.europa.eu/en/home/"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Council of the European Union
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://ec.europa.eu/commission/index_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Commission
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://curia.europa.eu/jcms/jcms/j_6/en/"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Court of Justice of the European Union (CJEU)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://www.ecb.europa.eu/home/html/index.en.html"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Central Bank (ECB)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://www.eca.europa.eu/en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Court of Auditors (ECA)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://eeas.europa.eu/headquarters/headquarters-homepage_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European External Action Service (EEAS)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://www.eesc.europa.eu/?i=portal.en.home"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Economic and Social Committee (EESC)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="http://cor.europa.eu/en/"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Committee of the Regions (CoR)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://www.eib.org/en/index.htm"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Investment Bank (EIB)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://www.ombudsman.europa.eu/en/home"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Ombudsman
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://secure.edps.europa.eu/EDPSWEB/edps/EDPS?lang=en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Data Protection Supervisor (EDPS)
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://edpb.europa.eu/edpb_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Data Protection Board
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://epso.europa.eu/home_en"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    European Personnel Selection Office
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://op.europa.eu/en/home"
                                   class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Publications Office of the European Union
                                </a>
                            </li>
                            <li class="ecl-footer-standardised__list-item">
                                <a href="https://european-union.europa.eu/institutions-law-budget/institutions-and-bodies/institutions-and-bodies-profiles_en?f%5B0%5D=oe_organisation_eu_type%3Ahttp%3A//publications.europa.eu/resource/authority/corporate-body-classification/AGENCY_DEC&amp;f%5B1%5D=oe_organisation_eu_type%3Ahttp%3A//publications.europa.eu/resource/authority/corporate-body-classification/AGENCY_EXEC&amp;f%5B2%5D=oe_organisation_eu_type%3Ahttp%3A//publications.europa.eu/resource/authority/corporate-body-classification/EU_JU"
                                class="ecl-link ecl-link--standalone ecl-footer-standardised__link">
                                    Agencies
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

       <!-- <script src="/js/eu/runtime~main.5693af20.js"></script>
        <script src="/js/eu/vendors.ef9f5d65.chunk.js"></script>
        <script src="/js/eu/main.948b526c.chunk.js"></script>
        <script defer src="https://europa.eu/webtools/load.js?globan=1110" type="text/javascript"></script> -->
    </body>
</html>